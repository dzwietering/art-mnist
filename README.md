# Data, the graal and Achilles' heel of AI

![alt text](images/Cat.png "IBM ART")
# Your AI might tell this image is not a cat !


## Learn how to build a neural network for Handwritten digit recognition with MNIST using IBM Watson Studio Jupyter Notebook in Python & how to test, harness its robustness against fake data leveraging IBM Adversarial robustness Toolbox (ART) open source library - for Data Scientist.

![alt text](images/Handwritten.png "IBM Watson Studio CNN for Data Scientist")

This tutorial shows IBM Watson Studio framework capabilities to create Jupyter notebook leveraging Keras ML framework to create a convolutional neural network (CNN) model that is build & trained & improved in term of robustness & accuracy with IBM Watson Machine Learning Toolbox ART & IBM Watson ML capabilities.

## Introduction
Watson Studio provides you with the environment and tools to solve your business problems by collaboratively working with data. You can choose the tools you need to analyze and visualize data, to cleanse and shape data, to ingest streaming data, or to create, train, and deploy machine learning models.

This illustration shows how the architecture of Watson Studio is centered around the project. A project is where you organize your resources and work with data.


![alt text](images/Watson-Studio.png "IBM WS")



This tutorial will leverage IBM ART(Adversarial Robustness Toolbox) open source library
availalbe [here](https://github.com/IBM/adversarial-robustness-toolbox)

This is a library dedicated to adversarial machine learning. Its purpose is to allow rapid crafting and analysis of attacks and defense methods for machine learning models. The Adversarial Robustness Toolbox provides an implementation for many state-of-the-art methods for attacking and defending classifiers.

The ART toolbox is developed with the goal of helping developers better understand:

	Measuring model robustness
	Model hardening
	Runtime detection
	

![alt text](images/ART.png "IBM ART")


For more information you can read "Adversarial Robustness Toolbox v0.3.0" IBM research publication from Nicolae, Maria-Irina and Sinn, Mathieu and Tran, Minh~Ngoc and Rawat, Ambrish and Wistuba, Martin and Zantedeschi, Valentina and Baracaldo, Nathalie and Chen, Bryant and Ludwig, Heiko and Molloy, Ian and Edwards, Ben
available [here](https://arxiv.org/pdf/1807.01069)


##  Step 1 - Create your notebook

Once logged in the Watson Studio platform within a project create a new Notebook Select a name for your notebook and python language
For the runtime select the biggest one you can depending on the price (free or not)




![alt text](images/Notebook.png "IBM WS")

You're now ready to start writing your Handwritten digit Keras CNN and test it.


![alt text](images/Notebook1.png "IBM WS")

Start "copy/paste" the following code into the first cell then click the run button to view the output of the excecution of your code 
the goal of this snippet is to install the ART libs into our environment
and import all required classes.
Keras version will be 2.2.4 i nthis tutorial.

```
!pip install adversarial-robustness-toolbox

from keras.models import load_model
from art.utils import load_dataset
from art.classifiers import KerasClassifier
from art.attacks.fast_gradient import FastGradientMethod
from art.attacks.newtonfool import NewtonFool
from art.attacks.iterative_method import BasicIterativeMethod
from art.defences.adversarial_trainer import AdversarialTrainer

%matplotlib inline

import numpy as np
import matplotlib.pyplot as plt
import keras.backend as k
from keras.models import Sequential
from keras.layers import Dense, Flatten, Conv2D, MaxPool2D, Dropout
from keras.optimizers import RMSprop
import numpy as np
from matplotlib import pyplot as plt

import warnings
from numpy.random import seed
from tensorflow import set_random_seed
import tensorflow as tf
import random as rn


warnings.resetwarnings()  # Maybe somebody else is messing with the warnings system?
warnings.filterwarnings('ignore')  # Ignore everything
import keras
print("Keras version  :",keras.__version__)
```

You should get something like :


![alt text](images/Notebook2.png "IBM WS")


Now copy/paste the following utility functions to manipulate images
and run it


```
# Define a few utility functions

def setupSubPlots(numCols,maxRows):
    numRows=1+(maxRows-1)//numCols
    return plt.subplots(numRows,numCols,squeeze=False,figsize=(20,numRows/3*4))

def plotImage(img,axs,ix,numCols,title=None):
    ax=axs[ix//numCols][ix%numCols]
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    if title: 
        ax.set_title(title)
        ax.set_xticks([])
    return ax.imshow(img.squeeze())

def showImages(imgTable, limit, numCols=15):
    ''' Image plotting function, using a set number of columns
    '''
    limit=min(limit,len(imgTable))
    if limit<numCols: numCols=limit
    fig, axs = setupSubPlots(numCols,limit)
    for ix,img in enumerate(imgTable):
        if ix>=limit: break
        plotImage(img,axs,ix,numCols)
    return

def showImagesAccuracy(description,images, labels, predictList, maxShown=30, numCols=15):
    ''' Check Images prediction accuracy, count and display mismatches
    '''
    idx = 0
    errorsCount = 0
    shown = 0
    fig, axs = setupSubPlots(numCols,maxShown)
    for idx,img in enumerate(images):
        predicted = np.argmax(predictList[idx])
        actual = np.argmax(labels[idx])
        if predicted != actual:
            errorsCount += 1
            if shown < maxShown:
                # Plotting first samples of MNIST
                plotImage(img,axs,shown,numCols,"{}->{}".format(actual,predicted))
                shown += 1
    # Compute accuracy as a percentage         
    accuracy = (100-(errorsCount/len(images))*100)
    print("{} classifier succcess rate: {:.2f}%".format(description,accuracy))
    return accuracy
def get_file1(filename, url, path=None, extract=False):
    """
    Downloads a file from a URL if it not already in the cache. The file at indicated by `url` is downloaded to the
    path `path` (default is ~/.art/data). and given the name `filename`. Files in tar, tar.gz, tar.bz, and zip formats
    can also be extracted. This is a simplified version of the function with the same name in Keras.

    :param filename: Name of the file.
    :type filename: `str`
    :param url: Download URL.
    :type url: `str`
    :param path: Folder to store the download. If not specified, `~/.art/data` is used instead.
    :type: `str`
    :param extract: If true, tries to extract the archive.
    :type extract: `bool`
    :return: Path to the downloaded file.
    :rtype: `str`
    """
    if path is None:
        from art import DATA_PATH
        path_ = os.path.expanduser(DATA_PATH)
    else:
        path_ = os.path.expanduser(path)
    if not os.access(path_, os.W_OK):
        path_ = os.path.join('./', '.art')
    if not os.path.exists(path_):
        os.makedirs(path_)

    if extract:
        extract_path = os.path.join(path_, filename)
        full_path = extract_path + '.tar.gz'
    else:
        full_path = os.path.join(path_, filename)

    # Determine if dataset needs downloading
    download = not os.path.exists(full_path)

    if download:
        # logger.info('Downloading data from %s', url)
        error_msg = 'URL fetch failure on {}: {} -- {}'
        try:
            try:
                from six.moves.urllib.error import HTTPError, URLError
                from six.moves.urllib.request import urlretrieve

                urlretrieve(url, full_path)
            except HTTPError as e:
                raise Exception(error_msg.format(url, e.code, e.msg))
            except URLError as e:
                raise Exception(error_msg.format(url, e.errno, e.reason))
        except (Exception, KeyboardInterrupt):
            if os.path.exists(full_path):
                os.remove(full_path)
            raise

    if extract:
        if not os.path.exists(extract_path):
            _extract(full_path, path_)
        return extract_path

    return full_path


def preprocess(x, y, nb_classes=10, max_value=255):
    """Scales `x` to [0, 1] and converts `y` to class categorical confidences.

    :param x: Data instances
    :type x: `np.ndarray`
    :param y: Labels
    :type y: `np.ndarray`
    :param nb_classes: Number of classes in dataset
    :type nb_classes: `int`
    :param max_value: Original maximum allowed value for features
    :type max_value: `int`
    :return: rescaled values of `x`, `y`
    :rtype: `tuple`
    """
    x = x.astype('float32') / max_value
    y = to_categorical(y, nb_classes)

    return x, y

def to_categorical(labels, nb_classes=None):
    """
    Convert an array of labels to binary class matrix.

    :param labels: An array of integer labels of shape `(nb_samples,)`
    :type labels: `np.ndarray`
    :param nb_classes: The number of classes (possible labels)
    :type nb_classes: `int`
    :return: A binary matrix representation of `y` in the shape `(nb_samples, nb_classes)`
    :rtype: `np.ndarray`
    """
    labels = np.array(labels, dtype=np.int32)
    if not nb_classes:
        nb_classes = np.max(labels) + 1
    categorical = np.zeros((labels.shape[0], nb_classes), dtype=np.float32)
    categorical[np.arange(labels.shape[0]), np.squeeze(labels)] = 1
    return categorical

def load_mnist_data(raw=False):
    """Loads MNIST dataset from `DATA_PATH` or downloads it if necessary.

    :param raw: `True` if no preprocessing should be applied to the data. Otherwise, data is normalized to 1.
    :type raw: `bool`
    :return: `(x_train, y_train), (x_test, y_test), min, max`
    :rtype: `(np.ndarray, np.ndarray), (np.ndarray, np.ndarray), float, float`
    """
    # from art import DATA_PATH

    path = get_file1('mnist.npz', url='https://s3.amazonaws.com/img-datasets/mnist.npz')

    f = np.load(path)
    x_train = f['x_train']
    y_train = f['y_train']
    x_test = f['x_test']
    y_test = f['y_test']
    f.close()

    # Add channel axis
    min_, max_ = 0, 255
    if not raw:
        min_, max_ = 0., 1.
        x_train = np.expand_dims(x_train, axis=3)
        x_test = np.expand_dims(x_test, axis=3)
        x_train, y_train = preprocess(x_train, y_train)
        x_test, y_test = preprocess(x_test, y_test)

    return (x_train, y_train), (x_test, y_test), min_, max_
   
```



##  Step 2 Prepare & Cleanse dataset

Load pre-shuffled MNIST data into train and test datasets
and create our CNN model to be trained.
Set the random seeds to common intiial state and therefore allows reproductability in numpy/tensorFlow training & predictions results.

```
# Read MNIST dataset
(x_train, y_train), (x_test, y_test), min_, max_ = load_mnist_data()

# The below is necessary for starting Numpy generated random numbers
# in a well-defined initial state.
np.random.seed(42)

# The below is necessary for starting core Python generated random numbers
# in a well-defined state.
rn.seed(12345)

# Force TensorFlow to use single thread.
# Multiple threads are a potential source of non-reproducible results.
# For further details, see: https://stackoverflow.com/questions/42022950/

# session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)

# The below tf.set_random_seed() will make random number generation
# in the TensorFlow backend have a well-defined initial state.
# For further details : https://www.tensorflow.org/api_docs/python/tf/set_random_seed

tf.set_random_seed(1234)

# sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
# k.set_session(sess)

# CREATE the CNN for our HandWritten Digits recognition
model = Sequential()
model.add(Conv2D(32, kernel_size=(3,3), activation='relu', input_shape=x_train.shape[1:]))
model.add(MaxPool2D(pool_size=(2,2)))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.25))
model.add(Dense(10, activation='softmax'))

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
model.summary()
```

You should see therefore a summary of our ANN model as shown hereafter
Note the total number of weights & bias to be determine during the training sequence and understand how complex can be to train very complex ANN with hundreds of layers (here we have only 3 !).

![alt text](images/Model.png "IBM model")


**Conv2D**
It is a 2D convolutional layer that we use to process the 2D MNIST input images. The first argument passed to the Conv2D() layer function is the number of output channels – in this case we have 32 output channels. 
The next input is the kernel_size, which in this case we have chosen to be a 3×3 moving window, next, the activation function is a rectified linear unit and finally we have to supply the model with the size of the input to the layer (which is declared in another part of the code.
Declaring the input shape is only required of the first layer. 
Keras is good enough to work out the size of the tensors flowing through the model from there.

**MaxPool2D** (Reduction matrix)

**Flatten** (Transform an input shape n*c*h*w into a n*(c*h*w) vector)

**Dense** (each neuron is linked to the ones of the n+1 layer)

**DropOut** (freeze some neurons to avoid overfitting)

**SoftMax** (Transform a scoring distribution in a Probability distribution)

**lr** (Learning rate, interval used for the gradient descent algorithm)

**loss** (Cost/loss function used for this neural network, e.g. linear regression)

**Activation** (function used to ponderate the output of the neurone)



##  Step 3 Build,test & run the CNN model


Now we are ready to train our defined model against the MNIST training data.
copy/paste & excute the code below :

```
# Train the Classifier wrapper

Original_Classifier = KerasClassifier((0,1),model,use_logits=False)
Original_Classifier.fit(x_train, y_train, batch_size=128, nb_epochs=10)
```

You should see the various iterations, epochs with an accuracy & loss values associated to the model :

<center>
**You should see different numbers on your notebook for the success ratio....
Fully explainable as the training & test dataset are selected randomly from MNIST
So nobody should have exaclty the same dataset from a statistical point of view but
differences must be very minor.**
</center>
	
![alt text](images/Model2.png "IBM model")


##  Step 4 Analyse results

Finally let's see what are the digits not recognized by our generated model.

```
# Test the classifier against fresh test data
predictions = Original_Classifier.predict(x_test)
# count false predictions and display 30 first mismatches
OriginalAccOnTest = showImagesAccuracy('Original images with Original ',x_test, y_test, predictions, 30)
```

You should see depending on the implemented model :
![alt text](images/Results.png "IBM WS")

Now let's start challenging our Hand Digit recognition model.
Thanks to the ART toolbox we have a full set of attacks & defense available to assess our classifier.


##  Step 5 Assess the quality of our Classifier 

The ART library contains implementations of the following evasion attacks:

	DeepFool (Moosavi-Dezfooli et al., 2015)
	Fast Gradient Method (Goodfellow et al., 2014)
	Basic Iterative Method (Kurakin et al., 2016)
	Projected Gradient Descent (Madry et al., 2017)
	Jacobian Saliency Map (Papernot et al., 2016)
	Universal Perturbation (Moosavi-Dezfooli et al., 2016)
	Virtual Adversarial Method (Miyato et al., 2015)
	C&W Attack (Carlini and Wagner, 2016)
	NewtonFool (Jang et al., 2017)
	
Let use one of them for this tutorial the Fast Gradient Method and assess our model against it.
MNIST training dataset is made of 60000 samples so to save some time let's create only 10% of modified data from it.

Copy/paste the following code :

```
# Create modified train sub dataset (100 first images) with noise on it.
# Craft adversarial samples with the FastGradient Method
adv_crafter_FGM = FastGradientMethod(Original_Classifier, eps=0.5)

# generate one tenth of images for training set and full for test set
x_train_adv_FGM = adv_crafter_FGM.generate(x_train[:(len(x_train)//10)])
x_test_adv_FGM = adv_crafter_FGM.generate(x_test[:len(x_test)])
showImages(x_train_adv_FGM,30,15)
```

you should see what are the modified images using this method (introdfucing some noise on it)


![alt text](images/FGM.png "IBM FGM")


Now assess our classifier against this new training dataset :
Copy /paste the following code and run it :

```
# Challenge the Classifier with FastGradient modified dataset
predictions = Original_Classifier.predict(x_test_adv_FGM[:len(x_test_adv_FGM)])
AccFGMOri=showImagesAccuracy('Adversarial FastGradient Images with Original ',x_test_adv_FGM, y_test, predictions, 0,15)
```

![alt text](images/FGM-result.png "IBM FGM")

the quality of our model goes down from 98,81% accuracy to 2.26% !!!!
and this is just a simple example so you cannot think about deplyoing this classifier in a real world production system with so much hole in term of accuracy !!

Again thanks to our ART toolbox to provide a set of librairies to increase during the training the accuracy and various known attacks.

Please refer to the gitHub article from ART to get a full sample on how to train more efficiently your classifier.

In this tutorila let's explore some interesting we can observe rapidly that proof the ART's added value.

To do so let's enrich our initial training data with the one we just created for the Fast Method Gradient Attack.


```
# Data augmentation: expand the training set with the adversarial samples
x_train_robust = np.append(x_train, x_train_adv_FGM, axis=0)
y_train_robust = np.append(y_train, y_train[:len(x_train_adv_FGM)], axis=0)

x_test_robust = np.append(x_test, x_test_adv_FGM[:len(x_test_adv_FGM)], axis=0)
y_test_robust = np.append(y_test, y_test[:len(x_test_adv_FGM)], axis=0)

# Create a new Classifier trained using this extended dataset
Robust_Classifier = KerasClassifier((0,1),model,use_logits=False)
Robust_Classifier.fit(x_train_robust, y_train_robust, nb_epochs=10, batch_size=128)
```

Again we now have a robust trained classifier as shown below :

![alt text](images/Robust_Model.png "IBM Robust model")

Let's make sure we've made some significant improvement with our Fast Gradient attack method ....copy/paste the code below :

```
# Challenge the Robust Classifier with FastGradient modified dataset
predictions = Robust_Classifier.predict(x_test_adv_FGM[:len(x_test_adv_FGM)])
showImagesAccuracy('Adversarial FastGradient Images with Robust',x_test_adv_FGM, y_test, predictions, 30)
```
and obviously we've made significant progress from 2.26% accuracy to 98.11% ....


![alt text](images/Robust_Result.png "IBM Robust model")


And Finally what about the original test images without attack artificats on its, did we also improved our model regarding "regular" test images ??????

```
predictions = Robust_Classifier.predict(x_test)
# count false predictions and display 30 first mismatches
RobustAccOnTest = showImagesAccuracy('Original Images with Robust Classifier',x_test, y_test, predictions, 30)
```

![alt text](images/Robust_Result_Test.png "IBM Robust model")


<center><p>
![alt text](images/End_Result.png "IBM Robust model")
</p></center>


As dreamt ... YES we did some increment in our tutorial but imagine mixing all these attacks together and the gain you can make on your model robustness & accuracy.
Definitively ART must be part of your Deep Learning journey to make your DL model business ready !

##  Step 5 : Conclusion

Last percent is the real challenge in Deep Learning and can make the difference between a good model and a deployed model.
Remember 100% accuracy is a myth, you will never reach it but make sure to be the nearest.


By looking at the overall digits for which our model was not working it appears clearly that our model is missing some handwritten common sense.
Because as human we've learned to draw digit with a pen on a paper we have on top of what we see the knwoledge of the digit drawing pattern that allows us to be 50% better than the generated model.
That's true for all deep learning technics to be accurate enough there is a need for more than just perception capabilities.
In a nutshell reasoning and more importantly common sense are required to be called **Artificial Intelligence**

Think of Autonomous Cars, Surgery robotics !!!

![alt text](images/AI.png "IBM WS")

How to build that, DL gurus are working on that, with technics similar to what we saw here called adversarial network where One ANN is in charge to challenge another reponsible for the modeling (in our case one ANN using autoencoder neurons willgenerate 1000 times more training data for each digit taking into account how to draw a digit and therefore the 2nd ANN will learn that from the training dataset.
Such technics are prooven step forwards in DL for business.

Please feel free to share this tutorial and provide me with your remarks, questions, I would appreciate


[LinkedIn](http://fr.linkedin.com/pub/jean-luc-collet/9/541/740)

Thanks !

Jean-Luc Collet
July 11, 2018