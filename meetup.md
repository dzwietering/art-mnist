[Meetup](https://www.meetup.com/IBM-Code-Amsterdam/events/259757413/)

[CNN visualization](https://www.cs.ryerson.ca/~aharley/vis/conv/flat.html)

[IBM ART demo](https://art-demo.mybluemix.net/)

[DARTS](http://adversarial-learning.princeton.edu/darts/)

[DARTS paper](https://arxiv.org/pdf/1707.08945.pdf)

[Recent news](https://arstechnica.com/information-technology/2019/04/researchers-trick-tesla-autopilot-into-steering-into-oncoming-traffic/)

[And again](https://gizmodo.com/how-a-piece-of-tape-tricked-a-tesla-into-reading-a-35mp-1841791417)

[Food for thought](https://www.thedrive.com/news/27976/people-are-being-arrested-and-jailed-due-to-hertz-erroneously-reporting-rental-cars-stolen-report)

[Adversarial speech](https://thegradient.pub/adversarial-speech/)

[IBM ART introduction](https://developer.ibm.com/open/projects/adversarial-robustness-toolbox/)

[IBM ART toolbox](https://github.com/IBM/adversarial-robustness-toolbox)

[Python notebook](https://gitlab.com/dzwietering/art-mnist)

[IBM Watson Studio](https://dataplatform.cloud.ibm.com/)

[IBM Cloud account](https://ibm.biz/Bd2Uvq)